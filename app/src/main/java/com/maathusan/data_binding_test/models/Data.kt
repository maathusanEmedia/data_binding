package com.maathusan.data_binding_test.models


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("address")
    val address: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: Image,
    @SerializedName("latitude")
    val latitude: String,
    @SerializedName("longitude")
    val longitude: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("postcode")
    val postcode: String,
    @SerializedName("title")
    val title: String
)