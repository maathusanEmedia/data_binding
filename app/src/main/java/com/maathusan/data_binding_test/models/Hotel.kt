package com.maathusan.data_binding_test.models


import com.google.gson.annotations.SerializedName
import com.maathusan.data_binding_test.models.Data

data class Hotel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("status")
    val status: Int
)