package com.maathusan.data_binding_test.apis

import com.maathusan.data_binding_test.models.Hotel
import com.maathusan.data_binding_test.utils.Constants.HOTELS_END_POINT
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

interface HotelsService {
    @Headers("Content-Type:application/json")
    @GET(HOTELS_END_POINT)
    suspend fun getAllHotels(): Response<Hotel>
}