package com.maathusan.data_binding_test.utils

object Constants {
    const val BASE_URL: String = "https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/"
    const val HOTELS_END_POINT: String = "hotels"
}