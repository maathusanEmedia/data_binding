package com.maathusan.data_binding_test.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maathusan.data_binding_test.models.Hotel
import com.maathusan.data_binding_test.repositories.HotelsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HotelsViewModel
@Inject constructor(private val repository: HotelsRepository) : ViewModel() {
    private val _response = MutableLiveData<Hotel>()

    val responseHotels: LiveData<Hotel> get() = _response

    init {
        getAllHotels()
    }

    private fun getAllHotels() = viewModelScope.launch {
        repository.getAllHotels().let { response ->
            if (response.isSuccessful) {
                _response.postValue(response.body())
            } else {
                Log.d("Hotel", "Error : ${response.errorBody()}")
            }
        }
    }
}