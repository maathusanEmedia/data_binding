package com.maathusan.data_binding_test.repositories

import com.maathusan.data_binding_test.apis.HotelsService
import javax.inject.Inject

class HotelsRepository
@Inject constructor(private val api: HotelsService) {
    suspend fun getAllHotels() = api.getAllHotels()
}
