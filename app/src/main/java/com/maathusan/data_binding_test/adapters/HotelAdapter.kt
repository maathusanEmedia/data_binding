package com.maathusan.data_binding_test.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maathusan.data_binding_test.databinding.AdapterHotelBinding
import com.maathusan.data_binding_test.models.Data
import com.maathusan.data_binding_test.models.Hotel

class HotelAdapter(var hotelList: List<Data>): RecyclerView.Adapter<HotelAdapter.ViewHolder>() {
   inner class ViewHolder(
       private val binding: AdapterHotelBinding
   ) : RecyclerView.ViewHolder(binding.root) {

       fun onBind(hotel: Data) {
           binding.hotelData = hotel
           binding.executePendingBindings()
       }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AdapterHotelBinding.inflate(
                LayoutInflater.from(parent.context),parent,false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hotel = hotelList[position]

        holder.onBind(hotel)
    }

    override fun getItemCount(): Int = hotelList.size
}