package com.maathusan.data_binding_test.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.maathusan.data_binding_test.adapters.HotelAdapter
import com.maathusan.data_binding_test.databinding.ActivityMainBinding
import com.maathusan.data_binding_test.viewmodels.HotelsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var hotelsAdapter: HotelAdapter
    private val viewModel: HotelsViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        configData()
    }

    private fun configData(){

        hotelsAdapter = HotelAdapter(listOf())
        binding.rvHotelList.apply {
            adapter = hotelsAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }

        viewModel.responseHotels.observe(this, { response ->
            hotelsAdapter.hotelList = response.data
            hotelsAdapter.notifyDataSetChanged()
        })
    }
}